<?php

require_once './Helper.class.php';
require_once './User.class.php';

if ( !User::isLoggedIn() ) {
  Helper::addError('Please login to access this page.');
  header('Location: ./login.php');
  die();
}
