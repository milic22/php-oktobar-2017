<?php

class Product {
  private $db;
  public $id;
  public $cat_id;
  public $title;
  public $description;
  public $img;
  public $price;
  public $created_at;
  public $updated_at;
  public $deleted_at;
  public $productImgPath = './img/products/';
  public $imgData;
  public $allowedImageTypes;
  public $allowedImageSize;

  function __construct($id = null) {
    $this->db = require './db.inc.php';
    $this->allowedImageTypes = [ 'image/jpeg', 'image/gif', 'image/png' ];
    $this->allowedImageSize = 2 * 1024 * 1024; //MB * KB * B

    if ($id) {
      $this->id = $id;
      $this->loadFromDatabase();
    }
  }

  public function loadFromDatabase() {
    $stmt_getProduct = $this->db->prepare("
      SELECT *
      FROM `products`
      WHERE `id` = :id
    ");
    $stmt_getProduct->execute([ ':id' => $this->id ]);
    $product = $stmt_getProduct->fetch();
    if($product) {
      $this->id = $product->id;
      $this->cat_id = $product->cat_id;
      $this->title = $product->title;
      $this->description = $product->description;
      $this->img = $product->img;
      $this->price = $product->price;
      $this->created_at = $product->created_at;
      $this->updated_at = $product->updated_at;
      $this->deleted_at = $product->deleted_at;
      return true;
    } else {
      return false;
    }
  }

  public function insert() {
    $this->makeImgDirectory();

    $stmt_insertProduct = $this->db->prepare("
      INSERT INTO `products`
      (`cat_id`, `title`, `description`, `img`, `price`)
      VALUES
      (:cat_id, :title, :description, :img, :price)
    ");
    $result = $stmt_insertProduct->execute([
      ':cat_id' => $this->cat_id,
      ':title' => $this->title,
      ':description' => $this->description,
      ':img' => $this->img,
      ':price' => $this->price
    ]);
    if ($result) {
      $this->id = $this->db->lastInsertId();
      $this->handleImage();
      $this->loadFromDatabase();
      return $result;
    } else {
      return false;
    }
  }

  public function handleImage() {
    require_once './Helper.class.php';

    if($this->imgData['error'] != 0) {
      Helper::addError('Error while uploading image.');
      return false;
    }

    if( !in_array($this->imgData['type'], $this->allowedImageTypes) ) {
      Helper::addError('Image type not allowed.');
      return false;
    }

    if( $this->imgData['size'] > $this->allowedImageSize ) {
      Helper::addError('Max image size is 2MB.');
      return false;
    }

    $ext = pathinfo($this->imgData['name'], PATHINFO_EXTENSION);
    $filename = $this->id . '.' . $ext;

    move_uploaded_file(
      $this->imgData['tmp_name'],
      $this->productImgPath . $filename
    );
    $this->img = $this->productImgPath . $filename;
    return $this->save();
  }

  public function update() {
    $stmt_updateProduct = $this->db->prepare("
      UPDATE `products`
      SET
        `cat_id` = :cat_id,
        `title` = :title,
        `description` = :description,
        `img` = :img,
        `price` = :price
      WHERE `id` = :id
    ");
    return $stmt_updateProduct->execute([
      ':cat_id' => $this->cat_id,
      ':title' => $this->title,
      ':description' => $this->description,
      ':img' => $this->img,
      ':price' => $this->price,
      ':id' => $this->id
    ]);
  }

  public function save() {
    if($this->id) {
      return $this->update();
    } else {
      return $this->insert();
    }
  }

  public function delete() {
    $stmt_deleteProduct = $this->db->prepare("
      UPDATE `products`
      SET
        `deleted_at` = NOW()
      WHERE `id` = :id
    ");
    $result = $stmt_deleteProduct->execute([ ':id' => $this->id ]);

    if($result) {
      $this->deleteCommentsForProduct();
      $this->deleteProductPhotos();
      $this->deleteCartsForProduct();
      return true;
    } else {
      return false;
    }
  }

  public function deleteCartsForProduct() {
    $stmt_deleteCartsForProduct = $this->db->prepare("
      DELETE
      FROM `carts`
      WHERE `product_id` = :product_id
    ");
    return $stmt_deleteCartsForProduct->execute([
      ':product_id' => $this->id
    ]);
  }

  public function deleteProductPhotos() {
    return unlink($this->img);
  }

  public function deleteCommentsForProduct() {
    $stmt_deleteCommentsForProduct = $this->db->prepare("
      UPDATE `comments`
      SET `deleted_at` = NOW()
      WHERE `product_id` = :product_id
    ");
    return $stmt_deleteCommentsForProduct->execute([
      ':product_id' => $this->id
    ]);
  }

  public function all($catId = null) {
    if($catId) {
      $stmt_getProducts = $this->db->prepare("
        SELECT *
        FROM `products`
        WHERE `deleted_at` IS NULL
        AND `cat_id` = :cat_id
      ");
      $stmt_getProducts->execute([ ':cat_id' => $catId ]);
      return $stmt_getProducts->fetchAll();
    } else {
      $stmt_getProducts = $this->db->prepare("
        SELECT *
        FROM `products`
        WHERE `deleted_at` IS NULL
      ");
      $stmt_getProducts->execute();
      return $stmt_getProducts->fetchAll();
    }
  }

  public function paginate($page = 1) {
    $config = require './config.inc.php';
    $offset = ($page - 1) * $config['products_per_page'];
    $stmt_getProducts = $this->db->prepare("
      SELECT *
      FROM `products`
      WHERE `deleted_at` IS NULL
      ORDER BY `id` DESC
      LIMIT {$config['products_per_page']}
      OFFSET $offset
    ");
    $stmt_getProducts->execute();
    return $stmt_getProducts->fetchAll();
  }

  public function numOfProducts() {
    $stmt_numOfProducts = $this->db->prepare("
      SELECT *
      FROM `products`
      WHERE `deleted_at` IS NULL
    ");
    $stmt_numOfProducts->execute();
    return $stmt_numOfProducts->rowCount();
  }

  public function addToCart($quantity = 1) {
    require_once './Helper.class.php';
    require_once './User.class.php';
    Helper::sessionStart();

    if (!User::isLoggedIn()) {
      Helper::addError('You need to be logged in to add product to cart.');
      return false;
    }

    $stmt_getProductFromCart = $this->db->prepare("
      SELECT *
      FROM `carts`
      WHERE `user_id` = :user_id
      AND `product_id` = :product_id
    ");
    $stmt_getProductFromCart->execute([
      ':user_id' => $_SESSION['user_id'],
      ':product_id' => $this->id
    ]);
    $productInCart = $stmt_getProductFromCart->fetch();

    if ( $productInCart ) {
      $newQuantity = $productInCart->quantity + $quantity;
      return $this->updateCartQuantity($productInCart->id, $newQuantity);
    } else {
      $stmt_addToCart = $this->db->prepare("
        INSERT INTO `carts`
        (`product_id`, `user_id`, `quantity`)
        VALUES
        (:product_id, :user_id, :quantity)
      ");
      return $stmt_addToCart->execute([
        ':product_id' => $this->id,
        ':user_id' => $_SESSION['user_id'],
        ':quantity' => $quantity
      ]);
    }
  }

  public function updateCartQuantity($cartId, $newQuantity) {
    $stmt_updateCartQuantity = $this->db->prepare("
      UPDATE `carts`
      SET `quantity` = :quantity
      WHERE `id` = :id
    ");
    return $stmt_updateCartQuantity->execute([
      ':id' => $cartId,
      ':quantity' => $newQuantity
    ]);
  }

  public function comments() {
    $stmt_commentsForProduct = $this->db->prepare("
      SELECT
        `comments`.`id`,
        `users`.`name`,
        `comments`.`body`,
        `comments`.`created_at`
      FROM `users`, `comments`
      WHERE `comments`.`user_id` = `users`.`id`
      AND `comments`.`product_id` = :product_id
      AND `comments`.`deleted_at` IS NULL
      ORDER BY `comments`.`created_at` DESC
    ");
    $stmt_commentsForProduct->execute([ ':product_id' => $this->id ]);
    return $stmt_commentsForProduct->fetchAll();
  }

  public function addComment($body) {
    require_once './Helper.class.php';
    if (!$body) {
      Helper::addError('You need to fill in comment body.');
      return false;
    }
    Helper::sessionStart();
    $stmt_addComment = $this->db->prepare("
      INSERT INTO `comments`
      (`user_id`, `product_id`, `body`)
      VALUES
      (:user_id, :product_id, :body)
    ");
    return $stmt_addComment->execute([
      ':user_id' => $_SESSION['user_id'],
      ':product_id' => $this->id,
      ':body' => $body
    ]);
  }

  public function deleteComment($id) {
    $stmt_deleteComment = $this->db->prepare("
      UPDATE `comments`
      SET `deleted_at` = NOW()
      WHERE `id` = :id
    ");
    return $stmt_deleteComment->execute([ ':id' => $id ]);
  }

  public function makeImgDirectory() {
    if( !file_exists($this->productImgPath) ) {
      mkdir($this->productImgPath, 0777, true);
    }
  }
}


