$(document).ready(function() {

  // sakrivanje alert prozora
  setTimeout(function() {
    $('.alert.alert-success').slideUp();
  }, 5000);

  // popovers
  $(function () {
    $('[data-toggle="popover"]').popover();
  });

});
