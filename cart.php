<?php include './user-only.php'; ?>
<?php require_once './Helper.class.php'; ?>

<?php

  $u = new User();
  
  if( isset($_POST['remove_from_cart']) ) {
    if( $u->removeFromCart($_POST['cart_id']) ) {
      Helper::addMessage('Product successfully removed from cart.');
    }
  }

  if( isset($_POST['update_quantity']) ) {
    if( $u->updateCartQuantity($_POST['cart_id'], $_POST['quantity']) ) {
      Helper::addMessage('Quantity updated successfully.');
    }
  }

  $products = $u->cart();

?>

<?php include './header.layout.php'; ?>

  <h1 class="mb-5">Cart</h1>

  <table class="table">
  <thead>
    <tr>
      <th>Product</th>
      <th>Price</th>
      <th>Quantity</th>
      <th>Total price</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <?php $total = 0; ?>
    <?php foreach($products as $product): ?>
      <?php $total += $product->price * $product->quantity; ?>
      <tr>
        <td nowrap><?php echo $product->title; ?></td>
        <td><?php echo $product->price; ?>.00RSD</td>
        <td>
          <form action="" method="post" class="form-inline">
            <input type="hidden" name="cart_id" value="<?php echo $product->id; ?>" />
            <div class="input-group col-md-8">
              <input type="number" name="quantity" value="<?php echo $product->quantity; ?>" min="1" class="form-control form-control-sm" />
              <div class="input-group-btn">
                <button class="btn btn-sm btn-outline-primary" name="update_quantity">
                  <i class="far fa-save"></i>
                  Update
                </button>
              </div>
            </div>
          </form>
        </td>
        <td><?php echo $product->price * $product->quantity; ?>.00RSD</td>
        <td>
          <form action="" method="post">
            <input type="hidden" name="cart_id" value="<?php echo $product->id; ?>" />
            <button class="btn btn-sm btn-outline-danger" name="remove_from_cart">
              <i class="far fa-trash-alt"></i>
              Remove from cart
            </button>
          </form>
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
  <tfoot>
    <tr>
      <th></th>
      <th></th>
      <th>Total</th>
      <th><?php echo $total; ?>.00 RSD</th>
      <th>
        <button class="btn btn-outline-success">
          <i class="far fa-credit-card"></i>
          Checkout
        </button>
      </th>
    </tr>
  </tfoot>
</table>

<?php include './footer.layout.php'; ?>
