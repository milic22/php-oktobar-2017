var poruka = document.getElementById('poruka');
var broj = document.getElementById('broj');
var pogodi = document.getElementById('pogodi');
var brojPokusaja = document.getElementById('brojPokusaja');
var istorija = document.getElementById('istorija');

var tajniBroj = Math.round( Math.random() * 1000 );
var brojac = 0;

pogodi.addEventListener('click', igraj);
broj.addEventListener('keydown', naPritisnutoDugme);


function naPritisnutoDugme(event) {
  if( event.key == "Enter" ) {
    event.preventDefault();
    igraj();
  }
}

function igraj() {
  var korisnikovBroj = broj.value;

  if (!korisnikovBroj) {
    alert('Broj je obavezan!');
    return false;
  }

  povecajBrojac();
  

  if( tajniBroj > korisnikovBroj ) {
    poruka.innerHTML = "Moj broj je veci!";
    broj.value = "";
    dodajUIstoriju(korisnikovBroj);
  }

  if( tajniBroj < korisnikovBroj ) {
    poruka.innerHTML = "Moj broj je manji!";
    broj.value = "";
    dodajUIstoriju(korisnikovBroj);
  }

  if( tajniBroj == korisnikovBroj ) {
    poruka.innerHTML = "Bravo! :)";
    var audio = new Audio('./applause8.mp3');
    audio.play();
  }
}

function povecajBrojac() {
  brojac++;
  brojPokusaja.innerHTML = brojac;
}

function dodajUIstoriju(broj) {
  istorija.innerHTML = "<div>Nije " + broj + "</div>" + istorija.innerHTML;
}
