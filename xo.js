$(document).ready(function() {

  var score = ['', '', '', '', '', '', '', '', ''];
  var nextPlayer = 'x';
  var winnerCombinations = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];

  function render() {
    for(var i = 0; i < score.length; i++) {
      $('div[data-id="' + i + '"]').text(score[i]);
    }
    $('.next-player').text(nextPlayer);
  }

  function switchPlayer() {
    if( nextPlayer == 'x' ) {
      nextPlayer = 'o';
    } else {
      nextPlayer = 'x';
    }
  }

  function areEqueal(niz) {
    if(score[niz[0]] == score[niz[1]] && score[niz[0]] == score[niz[2]]) {
      return score[niz[0]];
    }
    return false;
  }

  function checkForWinner() {
    for( var i = 0; i < winnerCombinations.length; i++ ) {
      var winner = areEqueal(winnerCombinations[i]);
      if(winner) {
        return winner;
      }
    }
    if ( score.indexOf('') == -1 ) {
      return 'no one!';
    }
    return false;
  }

  $('.tile').click(function() {
    if( $(this).text() == '' ) {
      score[$(this).data('id')] = nextPlayer;
      switchPlayer();
      render();
      var winner = checkForWinner();
      if (winner) {
        $('.winner-is').text(winner);
        $('.winner-container').css('display', 'flex');
      }
    }
  });

  $('.play-again').click(function() {
    score = ['', '', '', '', '', '', '', '', ''];
    nextPlayer = 'x';
    render();
    $('.winner-container').css('display', 'none');
  });

});