<?php

class Category {
  private $db;
  public $id;
  public $title;
  
  function __construct($id = null) {
    $this->db = require './db.inc.php';
    
    if($id) {
      $this->id = $id;
      $stmt_getCategory = $this->db->prepare("
        SELECT *
        FROM `categories`
        WHERE `id` = :id
      ");
      $stmt_getCategory->execute([
        ':id' => $this->id
      ]);

      $category = $stmt_getCategory->fetch();
      $this->title = $category->title;
    }
  }

  public function insert() {
    $stmt_insertCategory = $this->db->prepare("
      INSERT INTO `categories`
      (`title`)
      VALUES
      (:title)
    ");
    $result = $stmt_insertCategory->execute([
      ':title' => $this->title
    ]);
    $this->id = $this->db->lastInsertId();
    return $result;
  }

  public function all() {
    $stmt_getAllCategories = $this->db->prepare("
      SELECT
        `id`,
        `title`,
        (
            SELECT count(*)
            FROM `products`
            WHERE `cat_id` = `categories`.`id`
            AND `deleted_at` IS NULL
        ) as num_of_products
      FROM `categories`
      ORDER BY `title` ASC
    ");
    $stmt_getAllCategories->execute();
    return $stmt_getAllCategories->fetchAll();
  }

  public function delete() {
    $stmt_deleteCategory = $this->db->prepare("
      DELETE FROM `categories`
      WHERE `id` = :id
    ");
    return $stmt_deleteCategory->execute([
      ':id' => $this->id
    ]);
  }

  public function update() {
    $stmt_updateCategory = $this->db->prepare("
      UPDATE `categories`
      SET `title` = :title
      WHERE `id` = :id
    ");
    return $stmt_updateCategory->execute([
      ':id' => $this->id,
      ':title' => $this->title
    ]);
  }

  public function save() {
    if ($this->id) {
      return $this->update();
    } else {
      return $this->insert();
    }
  }
}
