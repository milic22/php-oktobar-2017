<?php

require_once './Helper.class.php';
require_once './User.class.php';

if ( !User::isLoggedIn() ) {
  Helper::addError('Please login to access this page.');
  header('Location: ./login.php');
  die();
}

$u = new User();
$u->getLoggedInUser();

if ( $u->acc_type != 'admin' ) {
  Helper::addError('You are not an administrator.');
  header('Location: ./index.php');
  die();
}
