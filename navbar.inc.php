<?php require_once './User.class.php'; ?>

<?php
  $u = new User();
  $u->getLoggedInUser();
?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
    <a class="navbar-brand" href="./">
      <i class="fas fa-shopping-cart"></i>
      WebShop
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="./">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./products.php">Products</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./contact.php">Contact</a>
        </li>
      </ul>
      <ul class="navbar-nav">
        <?php if(User::isLoggedIn()): ?>

          <li class="nav-item">
            <a href="./cart.php" class="nav-link">
              <i class="fas fa-shopping-cart"></i>
              <sup><span class="badge badge-pill badge-primary">
                <?php echo $u->numOfProductsInCart(); ?>
              </span></sup>
            </a>
          </li>
          
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="far fa-user"></i>
              <?php echo $u->name; ?>
              <small>(<?php echo $u->email; ?>)</small>
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="./update-profile.php">Update profile</a>
              <?php if ($u->acc_type == "admin"): ?>
                <div class="dropdown-header">Administration</div>
                <a class="dropdown-item" href="./manage-categories.php">Manage categories</a>
                <a class="dropdown-item" href="./add-product.php">Add Product</a>
              <?php endif; ?>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="./logout.php">Log out</a>
            </div>
          </li>
        <?php else: ?>
          <li class="nav-item">
            <a class="nav-link" href="./login.php">Login</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./register.php">Sign up</a>
          </li>
        <?php endif; ?>
      </ul>
    </div>
  </div>
</nav>