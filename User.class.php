<?php

class User {
  private $db;
  public $id;
  public $name;
  public $email;
  public $password;
  public $password_repeat;
  public $acc_type;
  public $created_at;
  public $updated_at;
  public $deleted_at;

  function __construct($id = null) {
    $this->db = require './db.inc.php';

    if ($id) {
      $this->id = $id;
      $this->loadFromDatabase();
    }
  }

  public function loadFromDatabase() {
    $stmt_getUser = $this->db->prepare("
      SELECT *
      FROM `users`
      WHERE `id` = :id
    ");
    $stmt_getUser->execute([ ':id' => $this->id ]);
    $user = $stmt_getUser->fetch();
    if($user) {
      $this->name = $user->name;
      $this->email = $user->email;
      $this->password = $user->password;
      $this->acc_type = $user->acc_type;
      $this->created_at = $user->created_at;
      $this->updated_at = $user->updated_at;
      $this->deleted_at = $user->deleted_at;
      return true;
    } else {
      return false;
    }
  }

  public function save() {
    if ($this->id) {
      return $this->update();
    } else {
      return $this->insert();
    }
  }

  public function insert() {
    $stmt_insertUser = $this->db->prepare("
      INSERT INTO `users`
      (`name`, `email`, `password`)
      VALUES
      (:name, :email, :password)
    ");
    $result = $stmt_insertUser->execute([
      ':name' => $this->name,
      ':email' => $this->email,
      ':password' => md5($this->password)
    ]);
    if ($result) {
      $this->id = $this->db->lastInsertId();
      $this->loadFromDatabase();
      return $result;
    } else {
      return false;
    }
  }

  public function update() {
    $stmt_updateUser = $this->db->prepare("
      UPDATE `users`
      SET
        `name` = :name,
        `email` = :email,
        `password` = :password,
        `acc_type` = :acc_type
      WHERE `id` = :id
    ");
    return $stmt_updateUser->execute([
      ':name' => $this->name,
      ':email' => $this->email,
      ':password' => md5($this->password),
      ':acc_type' => $this->acc_type,
      ':id' => $this->id
    ]);
  }

  public function delete() {
    $stmt_deleteUser = $this->db->prepare("
      UPDATE `users`
      SET `deleted_at` = NOW()
      WHERE `id` = :id
    ");
    return $stmt_deleteUser->execute([
      ':id' => $this->id
    ]);
  }

  public function all() {
    $stmt_getAllUsers = $this->db->prepare("
      SELECT *
      FROM `users`
      WHERE `deleted_at` IS NULL
      ORDER BY `created_at` DESC
    ");
    $stmt_getAllUsers->execute();
    return $stmt_getAllUsers->fetchAll();
  }

  public function login($email, $password) {
    $stmt_getUser = $this->db->prepare("
      SELECT *
      FROM `users`
      WHERE `email` = :email
      AND `password` = :password
    ");
    $stmt_getUser->execute([
      ':email' => $email,
      ':password' => md5($password)
    ]);
    $user = $stmt_getUser->fetch();
    if($user) {
      require_once './Helper.class.php';
      Helper::sessionStart();
      $_SESSION['user_id'] = $user->id;
      return true;
    } else {
      return false;
    }
  }

  public static function isLoggedIn() {
    require_once './Helper.class.php';
    Helper::sessionStart();
    return isset($_SESSION['user_id']);
  }

  public function getLoggedInUser() {
    require_once './Helper.class.php';
    Helper::sessionStart();
    if ( isset($_SESSION['user_id']) ) {
      $this->id = $_SESSION['user_id'];
      $this->loadFromDatabase();
      return true;
    } else {
      return false;
    }
  }

  public function cart() {
    require_once './Helper.class.php';
    Helper::sessionStart();

    $stmt_getCart = $this->db->prepare("
      SELECT
        `carts`.`id`,
        `products`.`title`,
        `products`.`price`,
        `carts`.`quantity`
      FROM `carts`, `products`
      WHERE `carts`.`product_id` = `products`.`id`
      AND `carts`.`user_id` = :user_id
    ");
    $stmt_getCart->execute([ ':user_id' => $_SESSION['user_id'] ]);
    return $stmt_getCart->fetchAll();
  }

  public function removeFromCart($cartId) {
    $stmt_removeFromCart = $this->db->prepare("
      DELETE
      FROM `carts`
      WHERE `id` = :id
    ");
    return $stmt_removeFromCart->execute([ ':id' => $cartId ]);
  }

  public function updateCartQuantity($cartId, $newQuantity) {
    $stmt_updateCartQuantity = $this->db->prepare("
      UPDATE `carts`
      SET `quantity` = :quantity
      WHERE `id` = :id
    ");
    return $stmt_updateCartQuantity->execute([
      ':id' => $cartId,
      ':quantity' => $newQuantity
    ]);
  }

  public function numOfProductsInCart() {
    require_once './Helper.class.php';
    Helper::sessionStart();

    $stmt_numOfProductsInCart = $this->db->prepare("
      SELECT *
      FROM `carts`
      WHERE `user_id` = :user_id
    ");
    $stmt_numOfProductsInCart->execute([
      ':user_id' => $_SESSION['user_id']
    ]);
    return $stmt_numOfProductsInCart->rowCount();
  }
}
