# Imate problem?
[Pisite ovde...](https://bitbucket.org/milic22/php-oktobar-2017/issues/new)

# Prazan Bootstrap projekat
[Download](https://bitbucket.org/milic22/php-oktobar-2017/get/bb6e5150efa4f39906f7dcd4440824f480749c68.zip)

# Preuzimanje odredjenog commit-a
* U glavnom meniju (sa leve strane) izaberite [commits](https://bitbucket.org/milic22/php-oktobar-2017/commits/all).
* Izaberite commit klikom na njegov kod.
* U adresi promenite rec "commits" u "get"
* Na kraju adrese dodajte ".zip"

# MySQL

## Kreiranje tabele
```mysql
CREATE TABLE `naziv_tabele` (
  `id` int AUTO_INCREMENT,
  `ime` varchar(30),
  `broj_godina` int,
  `datum_rodjenja` date,
  `pol` enum("muski", "zenski"),
  `test_polozen` BOOLEAN,
  `datum_unosa_ovog_reda` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
)
```


## Unosenje podataka
```mysql
INSERT INTO `naziv_tabele`
  (`ime`, `broj_godina`, `datum_rodjenja`, `pol`, `test_polozen`)
VALUES
  ("Moje ime", 26, "1991-01-22", "muski", true)
```
Za unosenje vise redova jednim upitom:
```mysql
INSERT INTO `naziv_tabele`
  (`ime`, `broj_godina`, `datum_rodjenja`, `pol`, `test_polozen`)
VALUES
("Moje ime", 26, "1991-01-22", "muski", true),
("Drugo ime", 20, "1992-12-22", "zenski", false)
```
### Vazno
int vrednosti i boolean vrednosti se unose bez navodnika.
Datum se unosi u formatu `godina-mesec-dan sati:minuti:sekunde`


## Izmena tabele
### Dodavanje kolone
Dodaje novu kolonu `date` u tabeli `renting` posle kolone `m_id`.
```mysql
ALTER TABLE `renting`
ADD COLUMN `date` datetime AFTER `m_id`
```
### Brisanje kolone
Brise kolonu `date`.
```mysql
ALTER TABLE `renting`
DROP COLUMN `date`
```
### Izmena tipa kolone
Menja tip kolone `date` u `varchar(50)`.
```mysql
ALTER TABLE `renting`
MODIFY COLUMN `date` varchar(50)
```

## Povezivanje tabela
```mysql
SELECT
  `users`.`name`,
  `users`.`email`,
  `movies`.`title`,
  `renting`.`paid`,
  `renting`.`returned`
FROM `renting`, `users`, `movies`
WHERE `users`.`id` = `renting`.`u_id`
AND `movies`.`id` = `renting`.`m_id`
```
## Brisanje podataka iz tabele
```mysql
DELETE
FROM `users`
WHERE `id` = 1
```

## Pretrazivanje po imenu
### Pocinje
```mysql
SELECT *
FROM `users`
WHERE `name` LIKE "ale%"
```
### Zavrsava se
```mysql
SELECT *
FROM `users`
WHERE `name` LIKE "%ndar"
```
### Sadrzi
```mysql
SELECT *
FROM `users`
WHERE `name` LIKE "%ksan%"
```
