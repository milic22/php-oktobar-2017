<?php

  require './admin-only.php';

  require_once './Category.class.php';
  require_once './Helper.class.php';

  if( isset($_POST['add']) ) {
    $new_cat = new Category();
    $new_cat->title = $_POST['title'];
    if( $new_cat->save() ) {
      Helper::addMessage('Category added successfully!');
    } else {
      Helper::addError('Failed to add category!');
    }
  }

  if( isset($_POST['delete']) ) {
    $cat = new Category($_POST['cat_id']);
    if( $cat->delete() ) {
      Helper::addMessage('Category deleted successfully!');
    } else {
      Helper::addError('Failed to delete category!');
    }
  }

  $c = new Category();
  $categories = $c->all();

?>

<?php include './header.layout.php'; ?>

  <h1>Manage categories</h1>


  <form action="./manage-categories.php" method="post" class="form-inline mt-5">
    <div class="form-group mx-sm-3">
      <input type="text" name="title" class="form-control" placeholder="Enter category title here" />
    </div>
    <button name="add" class="btn btn-primary">
      <i class="far fa-save"></i>
      Add category
    </button>
  </form>


  <table class="table table-hover mt-5">
    <thead class="thead-dark">
      <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>

      <?php foreach($categories as $category): ?>
        <tr>
          <th><?php echo $category->id; ?></th>
          <td><?php echo $category->title; ?></td>
          <td>
            <form
              class="mb-0"
              action="./manage-categories.php"
              method="post">
              <input
                type="hidden"
                name="cat_id"
                value="<?php echo $category->id; ?>" />
              <button
                name="delete"
                class="btn btn-sm btn-danger">
                  <i class="far fa-trash-alt"></i>
                  Delete
              </button>
            </form>
          </td>
        </tr>
      <?php endforeach; ?>
      

    </tbody>
  </table>

<?php include './footer.layout.php'; ?>